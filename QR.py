# coding:utf-8

import os
import subprocess
import sys
from flask import Flask, render_template, request, redirect, url_for
from flask.ext.qrcode import QRcode
from werkzeug.utils import secure_filename

reload(sys)
sys.setdefaultencoding('utf-8')

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = 'files\\upload'
ALLOWED_EXTENSIONS = set(['jpg','jpeg'])

app = Flask(__name__)
QRcode(app)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['APP_ROOT'] = APP_ROOT


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/show', methods=['GET','POST'])
def showQR():
    entry = request.form['entry']
    return render_template('show.html', entry=entry)


@app.route('/encode')
def encode():
    return render_template('encode.html')


@app.route('/decode')
def decode():
    return render_template('decode.html')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def decodeQR(file):
    command = '{0}\zbarimg.exe -D {1}'.format(os.path.join(app.config['APP_ROOT'],'files'), file)
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    proc.wait()
    result = proc.communicate()[0].split(':')[1]
    try:
        result.decode('utf-8')
    except:
        print('sorry')
    os.remove(file)
    return result


@app.route('/upload', methods=['POST'])
def fileUpload():
    file = request.files['file']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        try:
            filePath = os.path.join(app.config['APP_ROOT'], app.config['UPLOAD_FOLDER'], filename)
            file.save(filePath)
        except:
            print("sorry")
        result = decodeQR(filePath)
    else:
        result = 'На данный момент распознаются только файлы в формате jpg, jpeg. Загрузите файл в указанном формате'
    return render_template('showresencode.html', result=result)


if __name__ == '__main__':
    app.run(debug = True)
